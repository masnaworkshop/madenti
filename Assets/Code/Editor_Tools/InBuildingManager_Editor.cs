﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(InBuildingManager))]
public class InBuildingManager_Editor : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		InBuildingManager myScript = (InBuildingManager)target;
		if (GUILayout.Button("save map data"))
		{
			myScript.saveData();
		}
		if(GUILayout.Button("Clear map"))
		{
			myScript.clearMap();
		}
		if(GUILayout.Button("Load map data"))
		{
			myScript.loadData();
		}
	}
}
