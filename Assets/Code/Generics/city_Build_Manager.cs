﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class city_Build_Manager : MonoBehaviour {

    //مثل الاب للعنصر هذا راح نستخدم الاوبجيكت هذا مره وحده فهذي طريقه عشان نسوي له رفرنس سريع
    public static city_Build_Manager instance;//singleton pattern

    // building info
    public Building chosenBuilding;

    // Use this for initialization
    void Awake()
    {
        if (instance != null)
            Destroy(this);

        instance = this;
    }
    public void SetCurrentBuilding(Building input)
    {
        this.chosenBuilding = input;
    }


}//end script
