﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu(fileName = "New Node", menuName = "شجرة المهارات /مهاره جديدة")]
public class Skill_Node : ScriptableObject {

	public List<Skill_Node> _ConnectedNodes = new List<Skill_Node>();
	public Skill_Node _BaseNode;
	public SkillTree _SkillTree;
	public Skill_Enum Skill_State;
	public Sprite Icon;
	public GameObject SkillNodePref;
	public Sprite Unlocked;
	public Sprite Locked;
	public Sprite SkillActive;


}
