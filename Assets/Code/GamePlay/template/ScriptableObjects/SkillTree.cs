﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Skill Tree", menuName = "شجرة المهارات /شجرة جديدة")]
public class SkillTree : ScriptableObject {

	public string Skill_Tree = "";
	public Skill_Node BaseNode;
	
}
