﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Skill_Node_Logic : MonoBehaviour {

	public Skill_Node _Skill;
	public Skill_Node _BaseSkillNode;
	public GameObject[] ConnectedNodes;
	public GameObject[] Connectors;
	public Skill_Node_Logic previousNode;
	public int Counter;
	public int Connector_id;
	public Text NodeText;

	public Sprite Unlocked;
	public Sprite Locked;
	public Sprite Active;

	public Sprite Node_Unlocked;
	public Sprite Node_Locked;
	public Sprite Node_Active;
	void Start()
	{
		Counter = 0;
	}

	public void initNode()
	{

		name = _Skill.name;
        if (NodeText != null)
		NodeText.text = _Skill.name;
		if (_Skill._ConnectedNodes.Count > 0)
			foreach (Skill_Node skill in _Skill._ConnectedNodes)
			{
				Connectors[Counter].SetActive(true);
				GameObject Node = Instantiate(skill.SkillNodePref, ConnectedNodes[Counter].transform.position, Quaternion.identity,
					ConnectedNodes[Counter].transform);
				Node.name = skill.name;
				Node.GetComponent<Skill_Node_Logic>()._Skill = skill;
				Node.GetComponent<Skill_Node_Logic>().previousNode = this;
				Node.GetComponent<Skill_Node_Logic>().Connector_id = Counter;

				if (skill._BaseNode.Skill_State == Skill_Enum.Active)
				{
					skill.Skill_State = Skill_Enum.Unlocked;
					Node.GetComponent<Image>().sprite = skill.Unlocked;
					Connectors[Counter].GetComponent<Image>().sprite = Unlocked;
				}
				else
				{
					skill.Skill_State = Skill_Enum.Locked;
					Node.GetComponent<Image>().sprite = skill.Locked;
					Connectors[Counter].GetComponent<Image>().sprite = Locked;
				}
				Node.GetComponent<Skill_Node_Logic>().initNode();
				Counter++;
            }
	}

	
	public void OnClickFunction()
	{
		print(_Skill.Skill_State); 
		if (_Skill.Skill_State == Skill_Enum.Unlocked)
		{
			int counter = 0;
			_Skill.Skill_State = Skill_Enum.Active;
			GetComponent<Image>().sprite = _Skill.SkillActive;
			previousNode.Connectors[Connector_id].GetComponent<Image>().sprite = Active;
			foreach (Skill_Node skill in _Skill._ConnectedNodes)
			{
				skill.Skill_State = Skill_Enum.Unlocked;
				Connectors[counter].GetComponent<Image>().sprite = Unlocked;
				GameManager.instance.activate_Skill(_Skill);
			}

		}
		else
			print("Locked skill");
	}

	void Update()
	{
		if (_Skill._BaseNode.Skill_State == Skill_Enum.Active && _Skill.Skill_State != Skill_Enum.Active)
		{
			GetComponent<Image>().sprite = _Skill.Unlocked;
			previousNode.Connectors[Connector_id].GetComponent<Image>().sprite = Unlocked;
		}
		else if (_Skill._BaseNode.Skill_State == Skill_Enum.Unlocked && _Skill.Skill_State != Skill_Enum.Active)
			GetComponent<Image>().sprite = _Skill.Locked;
		else if (_Skill._BaseNode.Skill_State == Skill_Enum.Locked && _Skill.Skill_State != Skill_Enum.Active)
			GetComponent<Image>().sprite = _Skill.Locked;



	}

}
