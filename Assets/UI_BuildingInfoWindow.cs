﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI_BuildingInfoWindow : MonoBehaviour {

	public TextMeshProUGUI buildingDescription;
	public TextMeshProUGUI buildingName;
	public TextMeshProUGUI buildingPrice;
}
