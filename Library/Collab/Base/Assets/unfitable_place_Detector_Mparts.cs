﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unfitable_place_Detector_Mparts : GroundNode {

    private Renderer[] rends;
    private Color[] StartColorsList;

    private void Start()
    {
        rends = CollectBuildingParts.buildingPartsInstance.getbuildingRenderers();
        StartColorsList = new Color[CollectBuildingParts.buildingPartsInstance.getbuildingParts().Length];// length for all building parts
        // save origin colors into the array
        for (int i = 0; i < StartColorsList.Length; i++)
            StartColorsList[i] = rends[i].material.color;

      //  foreach (Transform parts in CollectBuildingParts.buildingPartsInstance.getbuildingParts())
        //{ StartColorsList = parts.gameObject.GetComponent<Renderer>().material.color }
        //startColor = rend.material.color;
    }

    private void OnTriggerStay(Collider x)
    {
        if (x.tag == "Building")
        {
            print("triggered");
            foreach (Renderer parts in rends)
            { parts.material.color = Color.red; }
            correct_place = false;
        }
    }
    private void OnTriggerExit(Collider x)
    {
        if (x.tag == "Building")
        {
            for (int i = 0; i < StartColorsList.Length; i++)
               rends[i].material.color= StartColorsList[i];
            correct_place = true;

            /*this.gameObject.GetComponent<Renderer>().material.color = startColor;
            foreach (Transform parts in CollectBuildingParts.buildingPartsInstance.getbuildingParts())
            { parts.gameObject.GetComponent<Renderer>().material.color = startColor; }
            */
        }
    }
   
}

