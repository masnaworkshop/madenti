﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameSparks.Core;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Api.Responses;
using System.Linq;

public class General_UI : MonoBehaviour {

	public GameObject Listing;
	public GameObject BuyingContent;

	public GameObject SellItem;
	public GameObject SellContent;


	public static General_UI instance = null;

	// Use this for initialization
	void Start()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
			Destroy(this.gameObject);


		List<MarketPost> x = new List<MarketPost>();
		if (DB_Gamespark.instance != null)
		{
			DB_Gamespark.instance.Query_MarketPosts(RequestTypes.MarketBuyPanel, x, "rawOil");
		}
		StartCoroutine(_Update());
	}

	private void PopulateSellBoard()
	{
		Debug.Log("Received Market Data From GameSparks...");

		foreach (Item item in GameManager.instance.Storage.Values)
		{

			Instantiate(SellItem, SellContent.transform)
				.GetComponent<SellBoard_listing_logic>().initlize(item);
		}
	}

	public void ShowPanel(CanvasGroup panel)
	{
		if (panel.alpha > 0)
		{
			Exist_button(panel);
			return;
		}
		else
		{
			StartCoroutine(FadeImage(false, panel));
			panel.blocksRaycasts = true;
        }


	}
	public IEnumerator _Update()
	{
		while (true)
		{
			yield return new WaitForSeconds(0.5f);
			foreach (Item item in GameManager.instance.Storage.Values)
			{

				if (GameObject.Find(item.itemName + "_txt") != null)
					GameObject.Find(item.itemName + "_txt").GetComponent<Text>().text = item.amount.ToString();
			}
        }
	}
	IEnumerator FadeImage(bool fadeAway,CanvasGroup panel)
	{
		// fade from opaque to transparent
		if (fadeAway)
		{
			// loop over 1 second backwards
			for (float i = 1; i >= 0; i -= Time.deltaTime)
			{
				// set color with i as alpha
				panel.alpha = i;
				yield return null;
			}
			panel.alpha = 0;

		}
		// fade from transparent to opaque
		else
		{
			// loop over 1 second
			for (float i = 0; i <= 1; i += Time.deltaTime)
			{
				// set color with i as alpha
				panel.alpha = i;
				yield return null;
			}
			panel.alpha = 1;

		}
	}
	public void Exist_button(CanvasGroup panel)
	{
		if (panel.alpha < 0.5)
		{
			ShowPanel(panel);
			return;
		}
		else
		{
			StartCoroutine(FadeImage(true, panel));
			panel.blocksRaycasts = false;
        }
	}

	public void populateMarketBoard(LogEventResponse response)
	{
		List<GSData> posts = response.ScriptData.GetGSDataList("posts");
		Debug.Log("Received Market Data From GameSparks..." + response.ScriptData.GetGSDataList("posts").Count);
		List<GSData> pureList = posts.Distinct().ToList();

		foreach (GSData post in pureList)
		{
			string postid	 = post.GetString("itemID");
			string postname  = post.GetGSData("itemInfo").GetString("itemName");
			float postcount  = (float)post.GetGSData("itemInfo").GetInt("itemCount");
			Instantiate(Listing, BuyingContent.transform)
				.GetComponent<MarketPost_Logic>().initlize(new MarketPost(postid,postname,postcount));
		}
	}

	public void updateData()
	{
		PopulateSellBoard();
		StartCoroutine(_Update());
	}
}

