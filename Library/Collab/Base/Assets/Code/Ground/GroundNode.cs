﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GroundNode : MonoBehaviour{

    public static GameObject building; // that will be stored in the DB
    public bool correct_place =true;
    public static float x = 0f;
    GameObject current; //building that will be readyToBuild
    public Vector3 b_Offset { get { return new Vector3(0,2.5f,0); } }

    void OnMouseEnter()                                                                        
    {
       
        current = BuildManager.instance.GetCurrentBuilding();

		if (General_UI.GeneralUI_instance.selected && General_UI.GeneralUI_instance.readyToBuild)
		{
			building = (GameObject)Instantiate(current, transform.position + b_Offset, transform.rotation);
            General_UI.GeneralUI_instance.selected = false;
        }
		else if (General_UI.GeneralUI_instance.readyToBuild)
		{
                 building.GetComponent<Building_Logic>().Target = this.transform.position + b_Offset;

            // to go back normal build movement 
            // building.transform.position = transform.position + b_Offset;
        }
    }



    void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (correct_place == true && General_UI.GeneralUI_instance.readyToBuild)
            {
                if (EventSystem.current.IsPointerOverGameObject() == true)
                    return;
                    Do_Building();
        }
        else
            print("can not build");
    }
    }

    public void Do_Building()
    {
            StartCoroutine(coloring_building(building));
            General_UI.GeneralUI_instance.readyToBuild = false;
            correct_place = false; // to avoid stack building

            building.GetComponent<Building_Logic>().BuildingInfo.input.amount -= 200; // subtract the money 
            building.GetComponent<Building_Logic>().BuildingInfo.Node = this.gameObject.name;
            building.GetComponent<Building_Logic>().BuildingInfo.Position = this.transform.position;

            building = GameObject.FindGameObjectWithTag("BeforeBuilding");
            if (building.transform.childCount > 0)
            {
                foreach (Transform parts in CollectBuildingParts.buildingPartsInstance.buildingParts)
                { parts.gameObject.layer = 0; }
                foreach (Transform parts in CollectBuildingParts.buildingPartsInstance.buildingParts)
                { parts.gameObject.tag = "Building"; }
            }
            else
            {
                building.layer = 0;//defailt (raycast can hit)
                building.tag = "Building";
            }

            GameManager.instance.Add_Building(building.GetComponent<Building_Logic>().BuildingInfo);
    }
    
    private IEnumerator coloring_building(GameObject b)
    {
            b.GetComponent<Detector_And_Progress>().coloring_Build_progress();
        b.GetComponent<Building_Logic>().BuildingInfo.StartConstructing(b);
             yield return new WaitForSecondsRealtime((float)b.GetComponent<Building_Logic>().BuildingInfo.timeNeededToCunstruct().TotalSeconds);
        b.GetComponent<Detector_And_Progress>().Finish_Build_progress();
    }

}
