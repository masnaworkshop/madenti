﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// هذا منطق المباني 
/// </summary>
public class Building_Logic : MonoBehaviour {

	[SerializeField]
	public Building BuildingInfo;

	[SerializeField]
	public float AmountPerSecond =  1f;



	private IEnumerator LateStart()
	{
		yield return new WaitForSecondsRealtime(0.5f);
		if (BuildingInfo.RequiredSkill.Skill_State != Skill_Enum.Active)
		{
			print("You dont have the skill required to build ");
		}
		else if (BuildingInfo.RequiredSkill.Skill_State == Skill_Enum.Active)
		{
			switch (BuildingInfo.Building_Type)
			{
				case BuildingType.Collector:
					StartCoroutine(Collect());
					break;

				case BuildingType.Producer:
					StartCoroutine(Produce());
					break;
			}
		}
	}

	public void Reinitlize()
	{
		if (BuildingInfo.RequiredSkill.Skill_State != Skill_Enum.Active)
		{
			print("You dont have the skill required to build ");
			return;
		}
		switch (BuildingInfo.Building_Type)
		{
			case BuildingType.Collector:
				StartCoroutine(Collect());
				break;

			case BuildingType.Producer:
				StartCoroutine(Produce());
				break;

		}
	}


	private IEnumerator Produce()
	{
		while (true)
		{
			//وبهذي الطريقه نجيب الرفرينس للمخرج المطلوب والي هو فالمبنى الحالي خام حديد
			yield return new WaitForSecondsRealtime(1);
			//اذا فيه متطلب للغرض شيل احذف من المتطلب للجديد
			if (GameManager.instance.Storage[BuildingInfo.input.itemName].amount  >=  BuildingInfo.output.requirement)
			{
				float required = BuildingInfo.output.requirement;
				GameManager.instance.Storage[BuildingInfo.input.itemName].amount  -= required;
				//انتج المطلوب
				GameManager.instance.Storage[BuildingInfo.output.itemName].amount += AmountPerSecond;
			}

		}
	}
	private IEnumerator Collect()
	{
		while (true)
		{
			//وبهذي الطريقه نجيب الرفرينس للمخرج المطلوب والي هو فالمبنى الحالي خام حديد
			yield return new WaitForSecondsRealtime(1);
			//انتج المطلوب
			GameManager.instance.Storage[BuildingInfo.output.itemName].amount += AmountPerSecond;


		}
	}
}
