﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillTreeLogic : MonoBehaviour {

	public SkillTree _SkillTree;
	public GameObject BaseNode;
	public Skill_Node SkillTreeBaseNode;


	void Start()
	{
			if (BaseNode!= null)
			{
				BaseNode.GetComponent<Skill_Node_Logic>()._Skill = SkillTreeBaseNode;
                BaseNode.GetComponent<Skill_Node_Logic>()._Skill = _SkillTree.BaseNode;
				BaseNode.GetComponent<Skill_Node_Logic>().initNode();
			}
	}

	
}
