﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class InBuildingManager : MonoBehaviour {



	public GameObject Block_prf;
	public GameObject Anchor;
	public GameObject ChosenMechine;
	public GameObject GhostingMechine;
	public Grid_Block_EventHandler CurrentGrid;
	public LayerMask GridLayer;
	public LayerMask MechineLayer;
	public orientation CurrentOriontal = orientation.Vertical;


	public static InBuildingManager instance;
	public float Y_Grid;
	public float X_Grid;
	public float scale;
	public GameObject Rails;
	public List<GameObject> Grid = new List<GameObject>();

	// Use this for initialization
	void Start () {
		if (instance == null)
			instance = this;

		Y_Grid = transform.localScale.z * scale;
		X_Grid = transform.localScale.x * scale;

	}
	bool Grid_Generator()
	{
		try
		{
			for (int x = 0; x < scale; x++)
			{
				for (int y = 0; y < scale; y++)
				{
					int xx = x;
					int yy = y;
					GameObject block = Instantiate(Block_prf, this.transform);
					block.transform.name = "Grid" + x + "x" + y;
					block.transform.position = new Vector3(x + Anchor.transform.GetChild(0).transform.position.x,
						0,
						y + Anchor.transform.GetChild(0).transform.position.z);
					Grid.Insert(0, block);
				}
			}
			return true;// if generator is successful 
		}
		catch
		{
			return false; // if generator failed
		}
	}
	// Update is called once per frame
	void Update ()
	{
		//if (Input.GetKeyDown(KeyCode.A))
		//{
		//	ChosenMechine = Rails;
		//	GhostingMechine = Instantiate(ChosenMechine, this.transform);

		//}
		if (Input.GetMouseButton(0) && CurrentGrid != null && ChosenMechine != null)
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray, out hit,GridLayer))
			{
				if(hit.collider.name.StartsWith("Grid"))
				CurrentGrid.ConstructMechine();
				print(hit.collider.name);
			}
		}
	
		
	}

	public void setRailsType(GameObject type)
	{
		if (GhostingMechine != null)
			Destroy(GhostingMechine);
		ChosenMechine = type;
		GhostingMechine = Instantiate(ChosenMechine, this.transform);
	}
	public void RotateRails()
	{
		if (GhostingMechine == null)
			return;
		GhostingMechine.transform.Rotate(new Vector3(0, 90, 0));
	}

}
