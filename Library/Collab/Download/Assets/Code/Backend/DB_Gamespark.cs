﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using GameSparks.Core;

public class DB_Gamespark : MonoBehaviour
{

	public static DB_Gamespark instance = null;

	// Use this for initialization
	void Start()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
			StartCoroutine(connectToGamespark());
		}
		else
			Destroy(this.gameObject);
	}

	private IEnumerator connectToGamespark()
	{

        
		yield return new WaitForSecondsRealtime(3f);
		new GameSparks.Api.Requests.DeviceAuthenticationRequest().Send((response) =>
		{
			if (!response.HasErrors)
			{
				Debug.Log("Device Authenticated...");
				if ((bool)response.NewPlayer)
					createPlayerRecords();
				else
					updateData();
				SceneManager.LoadScene("ONLINE_GAMESCENE");

			}
			else
			{
				StartCoroutine(connectToGamespark());
				Debug.Log("Error Authenticating Device...");
			}
		});
	}

	private void createPlayerRecords()
	{
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("PlayerCreation").SetEventAttribute("Storage",
		JsonConvert.SerializeObject(GameManager.instance.Storage)).Send((response) => {
			if (!response.HasErrors)
			{
				Debug.Log("Player created!!");
				updateData();

			}

			else
				Debug.Log("Items have not been saved");

		});
	}

	void transferdata(List<MarketPost> marketposts, List<GSData> posts)
	{

	}

	public void Query_MarketPosts(RequestTypes requestType, List<MarketPost> marketposts, string item)
	{

		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("QUERY_MARKETPLACE").SetEventAttribute("ITEMNAME", item).Send((response) => {
			if (!response.HasErrors)
			{
				switch (requestType)
				{
					case RequestTypes.MarketBuyPanel:
						{
							General_UI.GeneralUI_instance.populateMarketBoard(response);
							break;
						}
				}


			}
			else
			{
				Debug.Log("Error Loading Player Data...");
			}
		});
	}
	public void Post_To_Market(string itemName, long amount)
	{
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("POST_ITEM_TO_MARKET").SetEventAttribute(
	"ITEM_NAME", itemName).SetEventAttribute("ITEM_COUNT", amount)
	.Send((response) =>
	{
		if (!response.HasErrors)
		{
			print("POSTED To marketboard");

		}
		else
		{
			print("FAILED TO POST TO MARKETBOARD");
			GameManager.instance.Storage["itemName"].amount += amount;
		}
	});
	}
	private void updateData()
	{

		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LoadStorage").Send((response) => {
				if (!response.HasErrors)
				{
				Debug.Log("Items saved ");
					foreach(GSData _item in response.ScriptData.GetGSDataList("storage"))
						{
							Debug.Log("OKAY GENIUS : " + _item.GetString("Key") +"  \\ "+ JsonConvert.DeserializeObject<Item>
																						(_item.GetGSData("Value").JSON));
							if (GameManager.instance.Storage.ContainsKey(_item.GetString("Key")))
							{
								GameManager.instance.Storage[_item.GetGSData("Value").GetString("itemName")] = JsonConvert.DeserializeObject<Item>
																							(_item.GetGSData("Value").JSON);
							}
						}
				General_UI.GeneralUI_instance.updateData();
				}
				else
					Debug.Log("Items have not been saved");
			});
		}
	}
