﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour {

    public static BuildManager instance;//singleton pattern
    private GameObject currentBuilding;
    public GameObject standardBuildingPref;
    public GameObject BuildingPref2;

    void Awake()//singleton pattern
    {
        if (instance != null) { DestroyObject(this); }//دمرها احسن من انك تخليها
        instance = this;// reference to BuildManager
    }

    public void SetCurrentBuilding(GameObject b)
    {
        this.currentBuilding = b;
    }

    public GameObject GetCurrentBuilding()
    {
		if(currentBuilding == null)
		{
			print("Current building has not been set ! ");
		}
        return this.currentBuilding;
    }
}
