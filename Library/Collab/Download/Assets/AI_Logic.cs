﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AI_Logic : MonoBehaviour {


	private NavMeshAgent agent;
	[SerializeField]
	public GameObject target2;

	public Vector3 dist;
	// Use this for initialization
	void Start () {
		dist = target2.transform.position;
		agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {


		agent.SetDestination(dist);

	}
}
