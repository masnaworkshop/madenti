﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// storage + skill tree الي موجود
/// <summary>
/// هنا راح نحفظ جمييع القيم وجميع العناصر فاللعبه راح تطلب من هذا الكلاس
/// </summary>
public class GameManager : MonoBehaviour {

	public string jsonStorage;
	//مثل الاب للعنصر هذا راح نستخدم الاوبجيكت هذا مره وحده فهذي طريقه عشان نسوي له رفرنس سريع
	public static GameManager instance;//singleton pattern

    // building info
    public Building currentBuilding;
    public List<Building> building_Types_Array = new List<Building>();
    [Range(0, 5)]
    public int building_Index;
	


    public GameObject BuildingTemplate;                                          // ايش الهدف منها؟


	private string Map_Save_Data;

	// هذا المستودع الفعلي نقدر نسحب ونعدل القيم بشكل سهل جدا
	//مثلا
	// Storage['raw_iron'].amount +=10
	//وبهذي الطريقه نرفع عدد الحديد الخام فالمستودع بشكل سهل جدا
	public Dictionary<string, Item> Storage = new Dictionary<string, Item>();
	//public Dictionary<string, Skill_Node> Skills = new Dictionary<string, Skill_Node>();
	public List<Building> Buildings = new List<Building>();

	[SerializeField]
	public List<Item> items = new List<Item>();


	private string gameDataFileName = "data.json";

	// Use this for initialization
	void Awake ()
	{
		if (instance != null)
			Destroy(this);

		instance = this;

		//المفروض نغير العدد للمخزن فالذاكره , فالوقت الحالي كل مابدينا اللعبه راح نعطي نفسنا 5 خام حديد
		foreach (Item item in items)
		{
			print("Storage => ADDED " + item.name);
			Storage.Add(item.name, item);
		}

	}

	// Update is called once per frame
	void Update () {


	}
	void OnEnable()
	{
		SceneManager.sceneLoaded += loadScene;
	}
	void OnDisable()
	{
		SceneManager.sceneLoaded -= loadScene;
	}
    public void SetCurrentBuilding(Building input)
    {
     //   if (building_Index > building_Types_Array.Length) // to avoid the case :out side array exception
    //          building_Index = building_Types_Array.Length;
	
        this.currentBuilding = input;
    }

    public Building GetCurrentBuilding()
    {
        if (currentBuilding == null)
        {
            print("Current building has not been set ! ");
        }
        return this.currentBuilding;
        //return GetComponent<Building_Logic>().Choosed_BuildingInfo;   // does not work because @which building_logic in building do u want@
    }

    public void Add_Building(Building _building)
	{
		Buildings.Add(_building);
		print("Added building");
		//DB_Gamespark.instance.post_building(_building);
	}
	public void loadScene(Scene scene , LoadSceneMode mode)
	{
		if (scene.name == "ONLINE_SCENE")
		{
			Debug.Log("======Starting Loading Scene=====" + Buildings.Count);
			GameObject cam = GameObject.Find("Camera");
			cam.SetActive(false);
			foreach (Building buildingData in Buildings)
			{
				print(buildingData.BuildingModel.name);
				Debug.Log("LOADING GAME =>" + buildingData.name);
				GameObject node = GameObject.Find(buildingData.Node);
				GameObject b = Instantiate(BuildingTemplate, node.transform);
				//b.GetComponent<Building_Logic>().LoadToScene(buildingData);
			}
			cam.SetActive(true);
		}
	}


	public void SaveGameData(string dataToJson)
	{

	}

	public void activate_Skill(Skill_Node _Skill)
	{
		GameObject[] Buildings = GameObject.FindGameObjectsWithTag("Building");

		foreach(GameObject building in Buildings)
		{
			if(building.GetComponent<Building_Logic>().Choosed_BuildingInfo.RequiredSkill == _Skill)
			{
				building.GetComponent<Building_Logic>().Reinitlize();
            }
		}
	}

	
}
